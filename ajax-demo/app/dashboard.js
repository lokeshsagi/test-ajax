var App = window.App || {};

(function(app) {
  var deleteBtnCallback = function(e) {
    app.API.employee.delete(id, function(isOk) {
      if (isOk === true) {
        e.target.parentElement.parentElement.remove();
      }
    });
  };

  app.Dashboard = function() {
    this.employeeTable = document.getElementById("employeeTable");
    this.init = function() {
      this.load();
    };
    this.load = function() {
      var self = this;
      app.API.employee.list(function(isOk, emp_data) {
          if(!isOk) {
              alert("Failed to fetch data");
              return;
          }
        var table = new app.EmployeeTable(deleteBtnCallback);
        for (var emp of emp_data) {
          var trNode = table.getRow(emp.id, emp.name, emp.email, emp.phone);
          self.employeeTable.children[1].appendChild(trNode);
        }
      });
    };
  };
})(App);
