var App = window.App || {};
(function(app) {
  function EmployeeTable(deleteBtnCallback) {
    var rowTemplate = "<tr></tr>";
    var colTemplate = "<td></td>";

    var _createRow = function() {
      var node = document.createElement("tr");
      return node;
    };

    var _createColumn = function(innerText) {
      var node = document.createElement("td");
      node.innerHTML = "<span>" + innerText + "</span>";

      return node;
    };

    var _removeColumn = function(name, rowId, callback) {
      var node = document.createElement("td");
      var btn = document.createElement("button");
      btn.attributes.type = "button";
      btn.innerText = name;
      btn.addEventListener("click", callback, rowId);
      node.appendChild(btn);
      return node;
    };

    this.getRow = function(id, name, email, phone) {
      var tr_node = _createRow();
      tr_node.appendChild(_createColumn(id));
      tr_node.appendChild(_createColumn(name));
      tr_node.appendChild(_createColumn(email));
      tr_node.appendChild(_createColumn(phone));

      //tr_node.appendChild(_removeColumn("Remove", id, deleteBtnCallback.bind(null, event, id)));
      tr_node.appendChild(
        _removeColumn("Remove", id, function(e) {
          deleteBtnCallback(e, id);
        })
      );

      return tr_node;
    };
  }

  app.EmployeeTable = EmployeeTable;
})(App);
