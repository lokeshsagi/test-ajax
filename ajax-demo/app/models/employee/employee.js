var App = window.App || {};
(function(app) {

function Employee(_id, _name, _email, _phone) {
    
    this.id = _id;
    this.name =_name;
    this.email = _email;
    this.phone = _phone;
}

app.Employee = Employee;

} 


)(App);