var App = window.App || {};

(function(app) {
  app.init = function() {
    var btnLogin = document.getElementById("btnAddLogin");
    var userEmail = document.getElementById("userEmail");
    var userPw = document.getElementById("userPw");

    btnLogin.addEventListener("click", function(e) {
      // Store data

      localStorage.setItem("user", userEmail.value);
      localStorage.setItem("pwd", userPw.value);
      //sessionStorage.setItem('user',userEmail.value);

      // Get data
      var datauser = localStorage.getItem("user");
      var datauserpwd = localStorage.getItem("pwd");

      if (userEmail.value == "" && userPw.value == "") {
        return false;
      } else {
        if (userEmail.value == datauser && userPw.value == datauserpwd) {
          window.location.href = "dashboard.html";
        }
      }
    });
  };
})(App);
