var App = window.App || {};
App.api_root = "http://localhost:3100";
(function(w, app) {
  var ajax = {
    GET: function(url, callback) {
      var request = new XMLHttpRequest();
      request.open("GET", url);
      request.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
          var data = JSON.parse(this.response);
          callback(true, data);
        }
      };
      request.onerror = function() {
        callback(false, null);
      };

      request.send();
    },
    POST: function(url, data, callback) {
      var request = new XMLHttpRequest();
      request.open("POST", url);
      request.setRequestHeader("Content-type", "application/json");
      request.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
          var data = JSON.parse(this.response);
          callback(true, data);
        }
      };
      request.onerror = function() {
        callback(false, null);
      };
      request.send(JSON.stringify(data));
    }
  };

  app.API = {
    employee : {
        list : function(callback){
            ajax.GET(app.api_root + "/employee", callback)
        },
        add : function(data, callback){
            ajax.POST(app.api_root + "/employee", data, callback)
        },
        delete : function(id, callback) {
            ajax.DELETE(app.api_root + "/employee/" + id, callback)
        },
        update : function(id, data, callback) {
          ajax.PUT(app.api_root + "/employee/" + id, data, callback)
        }
    }
  }
})(window, App);
